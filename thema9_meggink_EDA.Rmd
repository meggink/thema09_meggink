---
title: "Results, Discussion & Conclusion"
author: "M. Eggink"
date: "9/11/2020"

header-includes:
   - \usepackage{longtable}
output:
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r}
# Load all the data sets
bcell <- read.csv("data/input_bcell.csv", sep = ",", header = T)
covid <- read.csv("data/input_covid.csv", sep = ",", header = T)
sars <- read.csv("data/input_sars.csv", sep = ",", header = T)
```

For this research we will only use the bcell dataset.

## Data

```{r, include=FALSE}
head(bcell)
```


\begin{longtable}[l]{l|l}
\caption{Data description} \\ \hline
\label{param_table}
$\textbf{Column name}$ & $\textbf{Descripton}$ \\ \hline
\endhead
parent protein id & parent protein ID \\ \hline
protein seq & parent protein sequence \\ \hline
start position & start position of peptide \\ \hline
end position & end position of peptide \\ \hline
peptide seq & peptide sequence \\ \hline
chou fasman & peptide feature, $\beta$ turn \\ \hline
emini & peptide feature, relative surface accessibility \\ \hline
kolaskar tongaonkar & peptide feature, antigenicity \\ \hline
parker & peptide feature, hydrophobicity \\ \hline
isoelectric point & protein feature \\ \hline
aromacity & protein feature \\ \hline
hydrophobicity & protein feature \\ \hline
stability & protein feature \\ \hline
target & antibody valence (target value) \\ \hline
\end{longtable}



Research question:
Can you create a machine learning algorithm that recognizes if b cells contain proteins that have antibody inducing activity?

The columns 'chou_fasman', 'emini', 'kolaskar_tongaonkar' and 'parker' are from the immune epitope database (IEDB). These look most relevant for this experiment, so these we will expore most in this EDA.

## Is there missing data?
```{r}
# Are there any NA's in the dataframe
sum(is.na(bcell))
```
There are no NA's in the dataframe, so that means that there is no missing data.

## Variation within the dataframe
```{r,include=FALSE}
library(ggplot2) 
library(gridExtra)
```

```{r}
ggplot(data = bcell, aes(target)) + 
  geom_histogram(binwidth = 1, fill = "azure3", colour = "white") + 
  labs(title = "Positive and Negative activity", x = "Target value (0 and 1)", y = "Count")
```
This figure shows the target value, this is a binary value. With this figure I expect that the value 0 means negative activity and the value 1 means positive activity. Because in the paper, wich belongs to the data, they explain that there are more negative than positve activity's.

```{r}
c.hist <- ggplot(data = bcell, aes(chou_fasman)) + 
  geom_histogram(fill = "darkblue", col = "white", bins = 50) + 
  labs(title = "Chou Fasman", x = "Chou Fasman value", y = "Count")
e.hist <- ggplot(data = bcell, aes(emini)) + 
  geom_histogram(fill = "darkblue", col = "white", bins = 50) + 
  labs(title = "Emini", x = "Emini value", y = "Count")
k.hist <- ggplot(data = bcell, aes(kolaskar_tongaonkar)) + 
  geom_histogram(fill = "darkblue", col = "white", bins = 50) + 
  labs(title = "Kolaskar Tongaonkar", x = "Kolaskar Tongaonkar value", y = "Count")
p.hist <- ggplot(data = bcell, aes(parker)) + 
  geom_histogram(fill = "darkblue", col = "white", bins = 50) + 
  labs(title = "Parker", x = "Parker value", y = "Count")

grid.arrange(c.hist, e.hist, k.hist, p.hist, top = "Histogram of different data")
```
This figure shows the distriubtion of the data, the data of chou fasman, kolaskar tongaonkar and parker are good divided. But the data of emini is packed together. 

```{r}
c.box <- ggplot(bcell, aes(x = target, y = chou_fasman, group = target)) + 
  geom_boxplot(fill = "pink") + 
  labs(title = "Chou Fasman", x = "Negative or Positive activity", y = "Chou Fasman value")
e.box <- ggplot(bcell, aes(x = target, y = emini, group = target)) + 
  geom_boxplot(fill = "pink") + 
  labs(title = "Emini", x = "Negative or Positive activity", y = "Emini value")
k.box <- ggplot(bcell, aes(x = target, y = kolaskar_tongaonkar, group = target)) + 
  geom_boxplot(fill = "pink") + 
  labs(title = "Kolaskar Tongaonkar", x = "Negative or Positive activity", 
       y = "Kolaskar Tongaonkar value")
p.box <- ggplot(bcell, aes(x = target, y = parker, group = target)) + 
  geom_boxplot(fill = "pink") + 
  labs(title = "Parker", x = "Negative or Positive activity", y = "Parker value")

grid.arrange(c.box, e.box, k.box, p.box, top = "Boxpot of different data")
```
In these boxplots there are a lot of outliers visible. These outliers kan be removed so the data will be cleaner and the differences between positives and negatives will be better visible.

## Remove outliers
```{r}
# Remove outliers chou_fasman
c.quantile <- quantile(bcell$chou_fasman)
c.iqr <- IQR(bcell$chou_fasman)
c.low <- c.quantile[2] - 1.5 * c.iqr
c.high <- c.quantile[4] + 1.5 * c.iqr
c.filterd <- subset(bcell, bcell$chou_fasman > c.low & bcell$chou_fasman < c.high)

# Remove outliers emini
e.quantile <- quantile(bcell$emini)
e.iqr <- IQR(bcell$emini)
e.low <- e.quantile[2] - 1.5 * e.iqr
e.high <- e.quantile[4] + 1.5 * e.iqr
e.filterd <- subset(bcell, bcell$emini > e.low & bcell$emini < e.high)

# Remove outliers kolaskar_tongaonkar
k.quantile <- quantile(bcell$kolaskar_tongaonkar)
k.iqr <- IQR(bcell$kolaskar_tongaonkar)
k.low <- k.quantile[2] - 1.5 * k.iqr
k.high <- k.quantile[4] + 1.5 * k.iqr
k.filterd <- subset(bcell, 
                    bcell$kolaskar_tongaonkar > k.low & bcell$kolaskar_tongaonkar < k.high)

# Remove outliers parker
p.quantile <- quantile(bcell$parker)
p.iqr <- IQR(bcell$parker)
p.low <- p.quantile[2] - 1.5 * p.iqr
p.high <- p.quantile[4] + 1.5 * p.iqr
p.filterd <- subset(bcell, bcell$parker > p.low & bcell$parker < p.high)

# New boxplots without outliers
cf.box <- ggplot(c.filterd, aes(x = target, y = chou_fasman, group = target)) + 
  geom_boxplot(fill = "pink") + 
  labs(title = "Chou Fasman", x = "Negative or Positive activity", y = "Chou Fasman value")
ef.box <- ggplot(e.filterd, aes(x = target, y= emini, group = target)) + 
  geom_boxplot(fill = "pink") + 
  labs(title = "Emini", x = "Negative or Positive activity", y = "Emini value")
kf.box <- ggplot(k.filterd, aes(x = target, y= kolaskar_tongaonkar, group = target)) + 
  geom_boxplot(fill = "pink") + 
  labs(title = "Kolaskar Tongaonkar", x = "Negative or Positive activity", 
       y = "Kolaskar Tongaonkar value")
pf.box <- ggplot(p.filterd, aes(x = target, y= parker, group = target)) + 
  geom_boxplot(fill = "pink") + 
  labs(title = "Parker", x = "Negative or Positive activity", y = "Parker value")

grid.arrange(cf.box, ef.box, kf.box, pf.box, 
             top = "Boxplot of different data with removed outliers")
```
Here the outliers are removed, this makes a much cleaner picture and we can see the differences better.

## Correlation

To see if there is any correlation between the variables these scatterplots have been made. Each variable is compaired to the other.

```{r fig.height = 2.8, fig.width = 5}
ggplot(bcell, aes(x = emini, y = chou_fasman, colour = target)) + 
  geom_point() + 
  labs(title = "Correlation between Chou Fasman and Emini", 
       x = "Emini value", y = "Chou Fasman value")
```

```{r fig.height = 2.8, fig.width = 5}
ggplot(bcell, aes(x = kolaskar_tongaonkar, y = chou_fasman, colour = target)) + 
  geom_point() + 
  labs(title = "Correlation between Kolaskar T. and Chou Fasman", 
       x = "Kolaskar Tongaonkar value", y = "Chou Fasman value")
```

```{r fig.height = 2.8, fig.width = 5}
ggplot(bcell, aes(x = parker, y = chou_fasman, colour = target)) + 
  geom_point() + 
  labs(title = "Correlation between Parker and Chou Fasman", 
       x = "Parker value", y = "Chou Fasman value")
```

```{r fig.height = 2.8, fig.width = 5}
ggplot(bcell, aes(x = emini, y = kolaskar_tongaonkar, colour = target)) + 
  geom_point() + 
  labs(title = "Correlation between Emini and Kolaskar Tongaonkar", 
       x = "Emini value", y = "Kolaskar Tongaonkar value")
```

```{r fig.height = 2.8, fig.width = 5}
ggplot(bcell, aes(x = emini, y = parker, colour = target)) + 
  geom_point() + 
  labs(title = "Correlation between Emini and Parker", 
       x = "Emini value", y = "Parker value")
```

```{r fig.height = 2.8, fig.width = 5}
ggplot(bcell, aes(x = kolaskar_tongaonkar, y = parker, colour = target)) + 
  geom_point() + 
  labs(title = "Correlation between Kolaskar Tongaonkar and Parker", 
       x = "Kolaskar Tongaonkar value", y = "Parker value")
```



