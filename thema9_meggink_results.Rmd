---
title: "Results, Discussion & Conclusion"
author: "M. Eggink"
date: "9/11/2020"

header-includes:
   - \usepackage{longtable}
output:
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r, include=FALSE}
# Load the data set
bcell <- read.csv("data/input_bcell.csv", sep = ",", header = T)
```

## Can you create a machine learning algorithm that recognizes if b cells contain proteins that have antibody inducing activity?

\newpage

\begin{longtable}[l]{l|l}
\caption{Data description} \\ \hline
\label{param_table}
$\textbf{Column name}$ & $\textbf{Descripton}$ \\ \hline
\endhead
parent protein id & parent protein ID \\ \hline
protein seq & parent protein sequence \\ \hline
start position & start position of peptide \\ \hline
end position & end position of peptide \\ \hline
peptide seq & peptide sequence \\ \hline
chou fasman & peptide feature, $\beta$ turn \\ \hline
emini & peptide feature, relative surface accessibility \\ \hline
kolaskar tongaonkar & peptide feature, antigenicity \\ \hline
parker & peptide feature, hydrophobicity \\ \hline
isoelectric point & protein feature \\ \hline
aromaticity & protein feature \\ \hline
hydrophobicity & protein feature \\ \hline
stability & protein feature \\ \hline
target & antibody valence (target value) \\ \hline
\end{longtable}

# Results
Before the data can be expored there cannot be any missing data (NA's). As seen below there is no missing data. So we can continue to select the most important columns. The columns 'chou_fasman', 'emini', 'kolaskar_tongaonkar' and 'parker' are from the immune epitope database (IEDB), and the cloumns "isoelectric_point', 'aromaticity', 'hydrophobicity' and 'stability' are also protein features. These will be usefull in the reserach because they are features of the protein, and this will decide if the protein has antibody inducing activity. Since only the protein features and target value are interesting for the project, the rest of the colums are removed from dataset. So the colums 'parent_protein_id', 'protein_seq', 'start_position', 'end_position' and 'peptide_seq' are removed.

```{r, include=FALSE}
# Is there missing data?
sum(is.na(bcell))
```

```{r, include=FALSE}
new.bcell <- bcell[6:14]
```

```{r,include=FALSE}
library(ggplot2) 
library(gridExtra)
library(plyr)
library(reshape2)
```

Below is a figure wich shows the count of positive and negative activity in the target column. False means there is negative activity and true means there is positive activity. In the dataset there is more negative activity than positive activity.

```{r echo=FALSE, fig.height = 3, fig.width = 4.5}

c.target <- count(new.bcell$target == 1)

ggplot(data = c.target, aes(x = x, y = freq)) + geom_bar(stat = "identity", fill = c("lightcoral", "lightblue1")) +
  labs(title = "Count negative or positive activity", x = "Activity", y = "Count")
```


```{r, echo=FALSE}
c.hist <- ggplot(data = new.bcell, aes(chou_fasman, group = target, fill = target, colour = target)) + 
  geom_histogram(bins = 50) + 
  labs(title = "Chou Fasman", x = "Chou Fasman value", y = "Count")
e.hist <- ggplot(data = new.bcell, aes(emini, group = target, fill = target, colour = target)) + 
  geom_histogram(bins = 50) + 
  labs(title = "Emini", x = "Emini value", y = "Count")
k.hist <- ggplot(data = new.bcell, aes(kolaskar_tongaonkar, group = target, fill = target, colour = target)) + 
  geom_histogram(bins = 50) + 
  labs(title = "Kolaskar Tongaonkar", x = "Kolaskar Tongaonkar value", y = "Count")
p.hist <- ggplot(data = new.bcell, aes(parker, group = target, fill = target, colour = target)) + 
  geom_histogram(bins = 50) + 
  labs(title = "Parker", x = "Parker value", y = "Count")
i.hist <- ggplot(data = new.bcell, aes(isoelectric_point, group = target, fill = target, colour = target)) + 
  geom_histogram(bins = 50) + 
  labs(title = "Isoelectric point", x = "Isoelectric point", y = "Count")
a.hist <- ggplot(data = new.bcell, aes(aromaticity, group = target, fill = target, colour = target)) + 
  geom_histogram(bins = 50) + 
  labs(title = "Aromaticity", x = "Aromaticity value", y = "Count")
h.hist <- ggplot(data = new.bcell, aes(hydrophobicity, group = target, fill = target, colour = target)) + 
  geom_histogram(bins = 50) + 
  labs(title = "Hydrophobicity", x = "Hydrophobicity value", y = "Count")
s.hist <- ggplot(data = new.bcell, aes(stability, group = target, fill = target, colour = target)) + 
  geom_histogram(bins = 50) + 
  labs(title = "Stability", x = "Stability value", y = "Count")

grid.arrange(c.hist, e.hist, k.hist, p.hist, i.hist, a.hist, h.hist, s.hist, top = "Distribution of the protein features")
```

In the figure above the distribution of the protein features are visualized. Here the protein features Chou Fasman, Kolaskar Tongaonkar and Parker have a normal distribution. Emini has a left skewed distribution. Isoelectric point, Aromaticity, Hydrophobicity and Stability are not normal distributed. 

```{r echo=FALSE}
c.box <- ggplot(new.bcell, aes(x = target, y = chou_fasman, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Chou Fasman")
e.box <- ggplot(new.bcell, aes(x = target, y = emini, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Emini")
k.box <- ggplot(new.bcell, aes(x = target, y = kolaskar_tongaonkar, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Kolaskar Tongaonkar")
p.box <- ggplot(new.bcell, aes(x = target, y = parker, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Parker")
i.box <- ggplot(new.bcell, aes(x = target, y = isoelectric_point, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Isoelectric point")
a.box <- ggplot(new.bcell, aes(x = target, y = aromaticity, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Aromaticity")
h.box <- ggplot(new.bcell, aes(x = target, y = hydrophobicity, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Hydrophobicity")
s.box <- ggplot(new.bcell, aes(x = target, y = stability, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Stability")

grid.arrange(c.box, e.box, k.box, p.box, i.box, a.box, h.box, s.box, top = "Boxpot of the protein features")
```
The figure above shows the distribution of the protein features per negative or positive activity. The pink boxplots are negative activity and the green boxplots are positive activitiy. All the protein features have many outliers, exept for isoelectric point. The differences are difficult to see between positive and negative activity.

```{r echo=FALSE}
# Remove outliers chou_fasman
c.quantile <- quantile(new.bcell$chou_fasman)
c.iqr <- IQR(new.bcell$chou_fasman)
c.low <- c.quantile[2] - 1.5 * c.iqr
c.high <- c.quantile[4] + 1.5 * c.iqr
c.filterd <- subset(new.bcell, new.bcell$chou_fasman > c.low & new.bcell$chou_fasman < c.high)

# Remove outliers emini
e.quantile <- quantile(new.bcell$emini)
e.iqr <- IQR(new.bcell$emini)
e.low <- e.quantile[2] - 1.5 * e.iqr
e.high <- e.quantile[4] + 1.5 * e.iqr
e.filterd <- subset(new.bcell, new.bcell$emini > e.low & new.bcell$emini < e.high)

# Remove outliers kolaskar_tongaonkar
k.quantile <- quantile(new.bcell$kolaskar_tongaonkar)
k.iqr <- IQR(new.bcell$kolaskar_tongaonkar)
k.low <- k.quantile[2] - 1.5 * k.iqr
k.high <- k.quantile[4] + 1.5 * k.iqr
k.filterd <- subset(new.bcell, 
                    new.bcell$kolaskar_tongaonkar > k.low & new.bcell$kolaskar_tongaonkar < k.high)

# Remove outliers parker
p.quantile <- quantile(new.bcell$parker)
p.iqr <- IQR(new.bcell$parker)
p.low <- p.quantile[2] - 1.5 * p.iqr
p.high <- p.quantile[4] + 1.5 * p.iqr
p.filterd <- subset(new.bcell, new.bcell$parker > p.low & new.bcell$parker < p.high)

# Remove outliers isoelectric_point
i.quantile <- quantile(new.bcell$isoelectric_point)
i.iqr <- IQR(new.bcell$isoelectric_point)
i.low <- i.quantile[2] - 1.5 * i.iqr
i.high <- i.quantile[4] + 1.5 * i.iqr
i.filterd <- subset(new.bcell, new.bcell$isoelectric_point > i.low & new.bcell$isoelectric_point < i.high)

# Remove outliers aromaticity
a.quantile <- quantile(new.bcell$aromaticity)
a.iqr <- IQR(new.bcell$aromaticity)
a.low <- a.quantile[2] - 1.5 * a.iqr
a.high <- a.quantile[4] + 1.5 * a.iqr
a.filterd <- subset(new.bcell, new.bcell$aromaticity > a.low & new.bcell$aromaticity < a.high)

# Remove outliers hydrophobicity
h.quantile <- quantile(new.bcell$hydrophobicity)
h.iqr <- IQR(new.bcell$hydrophobicity)
h.low <- h.quantile[2] - 1.5 * h.iqr
h.high <- h.quantile[4] + 1.5 * h.iqr
h.filterd <- subset(new.bcell, 
                    new.bcell$hydrophobicity > h.low & new.bcell$hydrophobicity < h.high)

# Remove outliers stability
s.quantile <- quantile(new.bcell$stability)
s.iqr <- IQR(new.bcell$parker)
s.low <- s.quantile[2] - 1.5 * s.iqr
s.high <- s.quantile[4] + 1.5 * s.iqr
s.filterd <- subset(new.bcell, new.bcell$stability > s.low & new.bcell$stability < s.high)

# New boxplots without outliers
cf.box <- ggplot(c.filterd, aes(x = target, y = chou_fasman, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Chou Fasman")
ef.box <- ggplot(e.filterd, aes(x = target, y= emini, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Emini")
kf.box <- ggplot(k.filterd, aes(x = target, y= kolaskar_tongaonkar, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Kolaskar Tongaonkar")
pf.box <- ggplot(p.filterd, aes(x = target, y= parker, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Parker")
if.box <- ggplot(i.filterd, aes(x = target, y = isoelectric_point, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Isoelectric point")
af.box <- ggplot(a.filterd, aes(x = target, y= aromaticity, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Aromaticity")
hf.box <- ggplot(h.filterd, aes(x = target, y= hydrophobicity, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Hydrophobicity")
sf.box <- ggplot(s.filterd, aes(x = target, y= stability, group = target)) + 
  geom_boxplot(fill = c("pink", "lightgreen")) + 
  labs(x = "Activity", y = "Stability")

grid.arrange(cf.box, ef.box, kf.box, pf.box, if.box, af.box, hf.box, sf.box, top = "Boxplot of different data with removed outliers")
```
In the figure above the distribution of the protein features per negative or positive is shown. The pink boxplots are negative activity and the green boxplots are positive activitiy. With removed outliers the differences are better visable. 

```{r echo=FALSE}
heat.bcell <- bcell[6:13]
cormat <- round(cor(heat.bcell),2)

melted_cormat <- melt(cormat)
p <-ggplot(data = melted_cormat, aes(x=Var1, y=Var2, fill=value)) + 
  geom_tile(colour = "white") +
  scale_fill_gradient(low="white", high="green4") + theme(axis.title.x = element_blank()) + theme(axis.title.y = element_blank())
p + ggtitle("correlation matrix of the protein features") + 
  theme(plot.title = element_text(size = 15, face = "bold", hjust = 0.5)) + 
  theme(axis.text.x = element_text(angle = 45, hjust = 1))

```
This is a correlation matrix of the protein features, as seen the protein features dont have a lot of correlation.

\newpage

# Discussion and Conclusion
When exporing the data there were a couple of things that caught the eye. First with the data there were a couple of columns that were not usefull for this experiment. Such as the start and end position, the sequence, etc. The parker value and hydrophobicity have the same meaning. The parker value is described as protein feature hydrophobicity, but this is from the IEBD. But the values are different so both are kept in the experiment. 

The next thing is that the outliers were removed, but with this you may remove usefull datapoints. So in further experiment the outliers will be taken along. It was also not possible to remove the outliers because you need the same amount of data points so you can$'$t combine all the dataframe$'$s with removed outliers.

Finally there is not a lot of correlation between the data when looked at the correlation matrix. For the value kolaskar tongaonkar it even say$'$s that there is no correlation with parker and little with chou fasman and emini. This could be that there is no correlation or it could be that there went something wrong with making the correlation matrix.














